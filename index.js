const express = require('express');
const axios = require('axios');
const qs = require('qs');
const https = require('https');
require('dotenv').config();  // Load environment variables from .env file

const app = express();
const port = process.env.PORT || 3000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());  // Middleware to parse JSON bodies

const agent = new https.Agent({
  rejectUnauthorized: false
});

// Define an array of base URLs
const baseUrls = process.env.BASE_URLS ? process.env.BASE_URLS.split(',') : ['https://103.149.200.136'];
let currentUrlIndex = 0;

// Function to get the next base URL in a round-robin fashion
function getNextBaseUrl() {
  const baseUrl = baseUrls[currentUrlIndex];
  currentUrlIndex = (currentUrlIndex + 1) % baseUrls.length;
  return baseUrl;
}

async function fetchToken() {
    const cookies = {
        'lb-session': 'None',
        'timestamp': 'None',
        'xy-arkose-session': 'None',
    };

    const headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': getNextBaseUrl(),
        'Priority': 'u=1, i',
        'Referer': `${getNextBaseUrl()}/v2/2.8.1/enforcement.f4e33fa62e17087194e4d15bf1cc505b.html`,
        'Sec-CH-UA': '"Not/A)Brand";v="8", "Chromium";v="126", "Google Chrome";v="126"',
        'Sec-CH-UA-Mobile': '?0',
        'Sec-CH-UA-Platform': '"Windows"',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36',
        'X-Ark-Esync-Value': '1721239200',
    };

    const cookieString = Object.entries(cookies).map(([key, value]) => `${key}=${value}`).join('; ');

    headers['Cookie'] = cookieString;

        const data = 'bda=eyJjdCI6Ii9OSS9rejFyVDd6am0vTTBzTks2SFVhL3lOV1lTWXJFT2V3emNOTnE0RHVGejRQdUFtWWJzRG5UbnlmYmxQd3pvRUN2a1hPS0JMcEVKNkxveFhpWWNpNHpDSkhHdGJmbVg0bC9tYzM0SnA5aE05TFdzYkh6RTBMR0pGY1pJSGlocFpSMDFDTG5VZTRRcFBUaE5IKzhHSFhVeGZGVVFJVUM1Y0g4dExlR3k2SmVkWFgvQ29EQVZaVnR2TVlqVURVbmJ5NHlFRnVoMFMwZEIvZWFnSG40c2Z0WjZUMmFuL2xDYVgzZkhqZkkvOHBidjFCbEs0RDdyOEFSbWNUcG4zTWoyQ2VXT3hCYVBuY2t2emxCM0M0UjJpRWdVVTRDTis2aWtqWUJtT29GTmlIaC9vMU5wd1hzRTRObFBIQmRod1pXMk04dEZBSjN2SEk4SlhQdkxPWTFCdUV5NDViMndzNmFpTnY2Y0ozZnREbVE3MWZ1K1pEMVZYTXVoaEtnZlJmTEN1UHJzZEZ5SzB3UzNqOTQ2bVVORXkxR3UzL2tiRjNuaHRXODhibFZFNXhESDZKTFFEbXZEajRlKzF1YnFtTUo4ODJ5L0lvUi9BTGE2Ry9meUNTZHZ2NUlzVXdkd1pvM05Vb1BBSEVPTllsSU1KMWJHSyt1a3NGZENoMm4xUmdEdlQ2SWRuVks4RE80cTNqRWlFdWlrcWdFSzNLMDFkdUNqM3hPc1YxSHNTR3grWGoxWXNNWG1DL1REd2FvV3ZSY1N4a1gvZ09GYlpQVGRSdEZnbmt3S0xJckNhU0ZRLzF2N3lwaVh1bkp2VUJPK05sQW1xNEtYZzJwcjRuRm1rMlZsMTMybXhLSkxBMHB1VkRkZ05CNkJ3WnVNV05QT1RWSWFLeUlIZEI4RmxXM0t3NXhHeFM5SzRZeUwyelQ0N0JOeVhvRUNqSDZmSFlPT2ZlaS80RWZRd09HY1JrUHdxT0FNcHFONzVzdWRFRUpXSm8vY1Z2em5Qdk0weXpaQWgwMlpJR0V1MnlGUTNWa3lwMG8yZW50dUx1RFJiNnBwMlJUQUZGQlFGSnVLbDBqOVZ3RHJyZUNOKzNhczRuSVVHN09JbVpPenZiMVV2OGthcVM5bWxaOXJ2NjRWdld5NnMvQXhtMkcvUWliOHFUdGtFSSt4ditQTC9YMmZ4Wnl0TEoyV2ZPWkFjc09xS3RKcWNxMlZUbVVUZk1wY0J0eWNsZmx6THJaSUFzcXJDM240VWkyTVA1OExMZkhiVkY2Y3pHZlRRc2w3SWJHVEtKTis0MDZ3MVlmeksvZk4relBCclJBcm14ZVJyMzFPZjd2SHRrRFNOVG0vTlk4M3hPZ3JYSUdDSExqeHFydVNaYzJ2YW1UUThMZW9GNUlSQWhjdS9JcElqSU55VkUwbmNlQ0JLU200bHJwSG16dHRrcHZ2YXNpVjZzT01NcUxvbklhYkxYamI3MElNMVJyclAvOU9CYUNnV3ZJbUVwY3ZSOXZ2NGZiU1hZYWVlUjd3a2RlRFVkQ2MxbnVoL0hwOWFOb01QVmdlMXdvekQ1d2Z0bFdnMnEwQWxsQXpianh2STc0Q2lxYVZSdzFVdXlBS2JLWjhhYTFUa2ZIU2dGem1zWVpKdFBhODdOQTFPNUV1ZWl2MXFQQ0hJUDFUR0JReDZkOFhOVEs0SkYwcjNpL09HM2RhRU9aV3VjUVZLbWdjcVJHeU54b200TityODRMRGEwNytSb1pPTXlKeTFuY2lURkM1d3QxZ2dueDg0VFhrdzVObTRzR3R6azZYT1hkQXQvZmZtQTdpdFFybTZLUm0yc1JhZ1Jha2QzTnRiYUxWVnQ3RlZaNUg0cEVlOHcyNG1tcUZCUFFzaGZJWHBZem5waFEyU3RGYXpsSTFEWUdVZ1orQ2F2YXdNaGtaQzF5eldQcEdIUUlwa1BXS1h3VjZ6bGEzTFpyRmM1YVl6c1ZxbWFVR2h3NkRpMTlrczhFMkpUaklwdXVBVTJTdktCS0tLSnV1NDFJb2ZEOXVqOVZoSDRQelpyeXNIcDh3bFN5Nk8zSFRLK3ljcUhtSVZnR3BOOGl2SUNqaHhMTG1PUzV1TS8rQ25vdk1oaXVLbzZ3ZWo2ZVhTZVFRckNzcEw2dTZZWGJPMmM1bHV0UVRHWWI3d0FZdnQ3M05LOTFrZURVMUs4M1EvWldBS2dtMmtLZTRwMFBadUk2Yk9TdHdqMnpMYStWbnI5V01pY2FiUjBBTGdDb1JyaHB4TnBIM0dQRk1icW1yWkpCalh6QXIyaGVpbHEyUnZFSE5lVDFFVUNkZlFzNDlxTzJSdnNVeEVNN040N2Z5Nk84NXI2YVlZR3JsTmxXbDF4em8ySGQvcDRjTy9xTTFicWxiSUU4cGZHV0JDRlpucmVERmlaSjNPbWp0czJJeUNpdU12ZzZqWHpFRHA4SnRhWTBRWldUcWZFUEVaOEhhaTM5SG05QlZ3aUtEVndGb2xKNFJHd05EU2p1aHQ5b2NzZGhQNHNEL0pWZWZqaUVGM0RSLzNiQjZOdTZPdndMeElBSXQ4TldVTzlCZEpNR0JWTTFtWitGb0pWSkdHclVNeUxUZDd0Ti85aDdVQnByeTlxcC9sSWdKbmNER0g2MWdFOG80Y2dGeFlPOHh1dTBRdmZyWDNyTGt0VnFiQXN4N0pmZktLWEtJVUlTY0dBcVhNUzNsMzhPbDhiRDBiYzZFblJVVVJ3dS9NZTAxRFBrWjQwUTZ2VW9GVFlRRXI4b3VZU2VrbGo4YXhWVEVtdDRpNkpjZUdybk1wQWxwV29LNzZVdGgzSFk1cG9YN3ZEVHpPYjRWTFJKY1d5R3AreTQ1a013UlRLc1JEVzB5YkFaWkVqR3pHSVo3ekJYODEvZS9DNUhjQ1JJT0VJYVU3d1FESVV6aXZVakw5ZEE0SXNQeEhFaXJWMHRqVTRYZDdqYXJKaDFqSXJDbjVnM0RiVWs0TjN6RS92K0RUMjZBRWxDSHM1RVViSURTaU43MkljaUZyaUU2NzB5WCtDMkJLVzZOdGtLN3YxVkZyS1BmRjYxSmZSK2lSTUtDT3Z3Ti85dnV1a3RrZHJreU4vclhmS2VRL2lHTnJua2NsMzNTL1ZhOTd2RUh6TkpWODVnalRVZUowd1lVVGg2cGJadlY0UVJzMHRXVkdONFVxZHVvbHQ3TjB0NWY4Qkk3Z0M0NkV3V0ttMGlZY3lNVGlFcGZJanVPWGVXVWpWRzdMTCtuYUE2Snk2VkdPNy9XcWcyK1p1QlV1K0M5cmhsRTZjRVNSQTQyYmFzZWVVTXJVWnljaEZxcVUwcTFwMHVtUDNiaU1VSFVqTjZsT0xPYUE1RFFIaG1pL2pJV09vNW5CN1VIT0M4Y0RQaWhaNG80bVJMQmw4MDEwT3BidGc3VEgraEpjQVFvRjliUmJJWk4vK0pJQW9DYUpjUlJiMW56T1JleUNsbTMrekdXZ1VNSnlxQ0x4djh2QnF2ZGd6cXBXcEZkTkhIUzlMYWxVbmYrSndnMXNYSFdrMm13NmhBSlNROEdRc1ZSUk82TGc1ejgvbWpDUGlxaTA4U3ZhTjYwejNuTDg4NkZESDB1d2pCcDBEa1czMUliYXIyak1ncVlwdlRFazlsRWI3S0RKMjN6NFVna0Z6c2dHanRFUkY0TEtvSDFiQ0p2VndKMmhIa2pMNFJDSFhpb1lWSmxjZ1lFbjg0ZGVIRmpKWHhBUnBjN05aZXlncVBUWmMrN0Z2VTc1WEgwdERDMGowS2dPaUZPTkNFaUs3L2ZaWXZuREE3YnpOOUdZa0JiRDNtekhqMTB1dFZ4VFZNTTc0S2JjcHIxZWphUm1wajNacGJ5b1E5dFljQy9xL0RicWt3NkVjQVNDN3JSUTVMMUQxWHpON3VPWk5qbVE2TkhLOGVSaGNldE5sR0ExY241RUhKNTZ1bEZpc2czTHY4K1dmZDM2VTZ5NndPODMvRk04a1ZrMXIzaFNsMHAvTndPTlNhSW9OcTVzRzNCbU1wUitWa1UzTURLajNIOXFmZTdDTGtNVDB4MUMzQStJQWNXNXVWZjV4Qy85dk05V2RNcUoyOWUrUDBmaC9CNzJnM0lTbG0rbnN2VlVKc3Ezd3hGSWMxU2VaL2YzcGZzMUNnb0hxUk1VL1c3dE9HVzBKTVF3VUZ0YnRyRndWM0YybDNTL045VzFBMk4rOG0rZlpkdkQ4blp1WmdsZGNCTGMyeHhVNy9WZUVDaW5LUDcvWHplSm4vbzRseDZzYVZManZYN3NzZ1czNUJrdXdibFMxWWR6VFBmZGE4aDVydU43b0I5MDkyY1JxWnB2MTdUK2FEZWdhczg0aThKNzNjcmhoVzJWUEpTWDNLVlEzczhoRlFDWDRGTFFWd2hEMG1NY2RrSjFTUG13UHFpa1lsRUp3TGM5c3JGSGtLTUx1dTc3WmN1alM2YnhTbG9ybzQ4Q1BHMjcreWRYaCt1Mkx2Zjl5SlUrSlh0YWY3ZEhDNmJ2WS81ZndrWnIzNlZ4cEMzNDZmT1ZNUGpRRFMzbTNnbHFBa0MxbWNpNjYyU2pRWUl2a0VXV2RFOGZzVVQ4bkg2bncwd1cxYXE3QTRraUJLaHlEeUxWY3lkRHdrNHR0TVI2djhGU1N5dENoL0RPbHZlQThhQXUvUjdDL05HZTlrcEMxUENEdFRtZjNnZ1hFWGZTMHJlVkp3eHdFRGkzSmpvbUJhNkdlMkNodk5LQTF0YldiQTFiZFlPQTNWNUJzdWxNUllHSHV2Q0dsTi9lMXplV3pzQXd0MlZvdGRjV2Q3M0NsdXFVSWhxcjJETnpqcytLRzRpSmZYaDk3SXZpaFVWbzhkbFpPQWR3QUY4YjJma2JyVFFxZ0huRGxHZngzbytxUnBMZ2xhVXVMa01OallkeFZmcmRQOFBmSjRSVm8raThFN0lEUWxwcVR1cFlHRjkvUGVlUmhOL3RvQVp6aXROL2hoRmIydmcxV3lVdTYvQ3Zxcmg4YjZReFhEYzN0dDZVSGlOMGdaa25hUUJHWDFVZGszU05BOG5iSjFGM05sQXdGdVdPWklPME04N0M5RVZyRWRwaDBIV0R3VnNpajdtWDJsVjdDU0laOU4yOHphNTAxZlRHSEdiWDdsejRtczZNb1hmcmFSWllsbFh1VVB1YVFlcHM5TEI1L0hXcTk2SWNHamJDVkhTdzFJYzF1QW5xOHRNU0ZWTDhWeEhndTh3MFM5SGloc21LeThRSG9oY1lzc0hHN0pYL2dybmkybjJVY2pNVlM5YlkxZEZJMG9nWXBRdHhDVEhKR1ZjSU1GUUN3akdqZFhkQ0VMV2w0MEErdnQ5SVgyaVIrZ1ZxK0FJVFpxaUlMZEM5aE5jeDBWbkJ5ZTB2Rkptc3RMSFU2S1VCK083VjNPK0pWYjJ4L05zSlhjQkEyN1kwZ2ljUURIb1diN01VYVFnbWZLSzBwT053QXM0NG1rT1pZMk4xeE51QTh0eUlJT3lDdDFxWmdYMlRkTStFajZCZVZHUENkSG52RlRhYUcrTG5qUlNrcDg0Tm5QeEZOcS9vSEdnRVVjcUpsUTN1aS9heWtyMkZwcGRmYTY2Zkp1SkpadUpXSkltZEYwL01XWFgrc3I2c3RCM0E5dWlBeFJZYzFPdHdBZnRnUlJLd2MyVXhqdmFSd1hxdkVMV0lENzN0aVdITHhETHYrNnNzekQ4eGpOK1pQcmRQZkVEK1pCeVJvR0lTZlBxRllwOG51UC9uRnJjSGFURk9oTnZLSXJLOWlHN0xkZXN5Um0wNkFkcUhvUDc3cVdTcDJRMjY2Tnhld3pvUW5LbGtQeDR6a202bERrUmk2K0w0RURCQWpCMnY1eXJBaXVXeUVGeXB2VUNwdkhnREk4TjVpbnp2TlQ2NnRkdTg4Q09uVXhOMVc0bTkyZ1FqYXhlQTNyaTdJUG1KbmNPY2VYVEtTMitIZGNhNDJaaHVMbXlCcmRidEhZemNtRjVMUzFOUUV6aUJzQmFUcEpzcHVkVWk5Mzh6NmptaDhoSmd1Q1RPenpNcm83TmxZVFVnMnVoSjlvajdzSVBEMWVzQ20wNTRYUE1CK0ExN0IyNUIvaEt5aUJSYlRWUVZSSDBibkUzSVhwWis4dk5Xemd3clBWMktyTXdWZHZ6RlRGQU8wVTd0cWtERUZlSkcxeElNUVJic0ZaWjg0K2tBUGZwZ1p1aGhNbjAxZ0xRb0V0RVlnRG1hWEFjNjc4U2VoUEhoL2tqbFVYNWR0K3JBUkNKR2RLYzNTbVFQdk9Nakx5NTEzcDhVYVFQUC9rbU91TDdSRG1ORHduT2l5WHZuT3VtTmdnN3ppNTNuSlBFVXlzaERLNHZSS3B1bDVpcjlRbWh2cXZYUERaWHVjVnV1VllObURWL2hCUnZxR1RldERWSzZYVS9zZEplOHE0c3RZYzJ5d2QrSU91aDd6S2NLWEN1V1FNVjUrcy9rbWdoNVpJck9QM3Z5eG5yL3ZzVjUwMERqYWVpbkxQY0dVaVdEMDR1YVVXSjlyWFEySUxsU05RRzJQVmpkSTVMd1R5NkhHdmZOSDFmTHhVVmdYakhxbUVLTEkyR0ZmNkRaM0dVZVRURTJXL25BQ3pDYkdWczN2VFg0WTVlL3Y1YUx0NTByUVRWbjdLY1lnZlFzdUVJWTByc0VoRzR5MlpXZ3RmK0U5dHk0dy9TMXpFRUtaK1dWUHRSd1JDeEtWK1YxMDJJS2YzbGdCMW9mdkZMcmhKSEM2U3Rjais0NUdLSlk5UWp5bGZSeEJrQ0gxK3VtNVkrYW9vQktmWkZzanJPZGtsYXNJZlJDWHFPVGd4NGtnRG91cTJhajRiSUZiVVI4MWlOVFF3aWErNFpUQjJ4eFdrNEM1S3Y1VTA5RVZFSnZGdGxtTzJKeGZMdndmcmg5eCtJU2VTZU1GZ1BVVS81TSthR0tVc1Q5a3V1RURrKzg2TmlleHFibmthZ2x0RFM3ZERJRXZreGFVdW1SR0FCRlZtL1ZIeDFmN1o0MCtxd3lzMjVUZGlpdVVqNlBoUFR4K1d1SWNXbFJjT2tzY1JzRGowRE43SmZGUUNBaUkwVE80QUkreG9SMXUyZ0VBVVdWYWRzSm15WUV1ZFBCdGM3VHFxOXlHakJFYktadG51V3JnL29WWlBNVno4bmJtZ3R2b3JkRE52UCs3Qm1DOW5ialBBUHpSc3JYTklwQjBMT0M1bXcyT2ZxcWsvMWFwL3diQ3BaZ0RWQ0UxeEFOaXR2TFhPT0JpR1YzTDNvcDFuTWpvUkJpb3VrZWdqQkFrdVhyV0pXVlFCVzRwOTgzVlJOOVBCRksrdXUzWXFVMzdvNDExZnpmZkZwS050eGI0cGpndWs5QWFzeW43NHlDVEszZEJIc1BKZUxFd09aMDhHSExoaFdZYXhnVXhiQU0xcHhLWi9JVHR0SkNYK0dIZzFBQnNhQTh5eDFmU3lEYnVEcFEvbFZzem9jU2pxZGh2V3VwRG41Ujd3Q0VVdjBuRFc0eE9uRDJ0ZmFHTzMzaVBpWDN4Szc3azhNR1BZMkNsR1U5OXBwbDJwU2RlUGl5SzV1ei92NkpHUWxqRFZqTkltZnRXU3lJL1c1UzlDWGpOZXlOYkFOSmlEUUNkMUx1cE5sbWxxQXZGTFhBT2FReG0zZHgxUURqSDlSMlJ0Wmd1SGpiVXo1MmFpcFFReDdRWGNCNFFiOFYrbTBBZlZUcTJ4NElUR2MreE0zLzJGQStkenpRYnFjWThnVUxGQWRKME52VytXSHl3eGhhaWFuNHArR1hweW5XVnpkSk5QWXVLR0o5L0dhenZ0TTFFbzc4empwR3lEN1hNL1gxNlV1N294SFd4Z0NrWUtlbU9tVXlMUW5BRFBpVStQRW9TMm53VGppYnBBWEhQWWFnZU1oeFdMQkJSb0Viamg2eEYzeng4djVKZnU1Zm5hU2svWjRDcTFFZWhyZXJLS0QyMFBIakFFSytiQitSbTZVdng5S2JsRzlhM3J4SXQ5UFlaVlhvdjJCSGRKQ2Vsek54dzFVZW42NXRHdHpzVTliMThLL3BQU0xuZlF3WVJOK1F3dTJhVm4zZ0Z1Qm1Eb2x3Z3BuWmEwQy9YZXUzeGplWmxFNFE0YnNKSENuOGZydStEN0k2NXl1Y3pQWUhxRWtBbWxYK0RJT1BtYnhGSGc4TnZKWjBmazcwTkJEa0xNWEJQTkFGMjlGd2FBMkVQUHpoTUhYMXN0K1YzekI2cElVUGhaWjFmRWNjNkREN2o1ckt0UlNNYXZDLy9BRnU4a1BOZ2paMEZYdU9OVkI2dVhyK3JTa29idmNVSTJqNkV6NmJWVmRFZnFVVzhWVTFsVzJ4ZGN0RytBY0NYMmdWbUYxTGlkSzdtaFAzTkxNOUJ4OTZZNTdXRXgrQlZrTTNXMDhYaVBWdTdnNnRnaUthOGhpa1NvK1E3M2M5K3pNU1h1L0pSOWNYb1d2MlZqSnRlWk9MZDhjamxJc2g4QTRsUWRiSXRUL051RVh2YWIybXUwTFZKcGxSWjdqNDcwQmdJa05MQTAwVEJJWGlaZWJYelFiMmh6S2JFRDY2YWpsaklORU5FMldsT21WKzBPSEFCT0o3MFZJRU5Wb2VZbWVsVm1rMU15QzZRUWdTemQyWU1EZE5CVlpiSE03TGUwRzZhc01wRFJLc25CVzJFUkYzc1FDMXVMTWV6NlJDSFIzV3l0ZzJ5SDdCR1ppNFkvWXlCbVdtTERkZnZIa2liVjVhWDk2NTh6bWtWem1KR2dQbFdQNDAvcGQ5QitBK2VDVzd0V0pWanIzeUhRalNRYzJxVHc3Q2VxYjlSMkZ3aEVoRlROa0NqYWY3R3h5NEE4ZUJpTWc1SFpvLzdweCtmV0w1VHhTVXhJcENOMDJMaXJNcFIrRjhFdVlHdDNYR053cUJSMnhBM3B2ZTZUL0tpT0JWMkZObWZOYWFaZ0pqdFNndjJ0a2xjSng4SnU5c2gwekdoaWl4aE9BQngrMjZ6dXVVVjJQc0VjaTJhbFF5cUVacWU5NWRqVXB1UEVBVzJWeDBMd3dPSldRNk9Zb0lkL2F3ZHNtdCs0MkJ2U2F3WHNJbmtXZlFpWVVvNVJZMGdxTzQ3SitoVjJ4d0tOVVhtdXNUTFdiVHA1N3RWZDNCeE5PL0VHNE9ITU43ZTVCVnpQOUd5RGY4aEt4Z0JvQnlNNG9rQ2c4eXFYVHZwcUpDNXppZlplNG8xZXdEWE9vUGtDZCtHc3ZJK3VGVHR0aWxHSUJwelVJTlhQeERZQXlPRzJyb1Z2WER4aTJ6QlFBeWgvU3FFM0ZnaFZKS3JDaHZiNzZRZmN3VndveE5JMzBoSmhQNm5Yd0dpS2tOelB6bkdXcFpTVjJ4UmN0YWE4bUhvTVg5Tm43eEJDNVBWSS8vMXBBWXhSRlZMME9rWmdOcUNoWWtmQnNpR3dJUTAzNUlzanhpSzdJR29jeXltdlhiZkFpeUV5eTFlOTNkV1JWcG9iclJXS2lpdFZna08xSGlxaEMwdlBoNmFnbk1yTElONmRoOWFwL0RDaTVLT2Z5UXlncnNqd2Z1UXBUWjhVcUFuWVBGQU1iL0ZxZEhZcmpKZTQ5MWVPTE4yaHVUNi83M2w0dU82clZBdTdtQ0dBUXQ3YWEzK3dRNTA4MEN3RTBDUnNyY2F0U3pWaW04cHR2dFhsWWI2dGJlRzF3Z251WjZ0M1FBMXFFYklzTmhiaWxkZ3ZkVmlsVDJVNFk2RS9IV1R4Z2ZabzZMS2EyNnNZaC9kekM3MXJRZzdjUW5jdUQ1cExJUFM2SWk3QlNLS21mOXRTaTkwYTZIaTN3MG00YVUzemVqWG0yNkdvTlcycU1UYVFnSDQycXdUazd5RkdUSUtpejQyUTA2ZFZFeVk2SXhRSDYzMHZVbmlzc0k2VStXS29YL1RrUytiT0tyRDhwbmNQMW5sblYzMkZaY29NYWZPbk5qcjM3S0hld3MxYmsvZE9nZUxYeVJJR1pGNXU5MFQ2bS94Y05YWmFZd2JYQlhicEVpd1dnVXhwbno5UW5sK0hXWlg5azZqWFZ5MEwxTGVUMFh4YlF2NS8rNWxlRzR3Rm5PQ1RNMnBOTnliMzhOakRWbEgyaVdiUGtKdm9oU3ZXelI5eUtjK2w5WkxtZndsbndhUTJyTnRhYysyY3BTYnZoRnlLRmEwUjFldXRVRWZlN0wrMklEeTBYakVmNHl6aW1FdG9meXdsY2FKZ3kzN3l1eGVpd1dWdVU0cEp4UWw2c3FpZWFaeWIzbEFrRGNiVGtIK2xIbzdnVXE1MXhFSytrUDNrN1E3V3k5RkIwNUlUZUNUMzI0cG1waXpja1RGeFRVVkc0Q1hMZmloeThuazRFOG5TbUtFNUgxd0s2SGJYNG10d1pZNEhvT3R5Q3d0RkZxSXVFSUMzZlUwbFB4QjJOZ2c2UXNaejlqY01CNDNNZk9jeFJYdUd0REg2cGhjKzkvcjFvYUtHKzE1elEvMUQ4RlNFNWNLeTJ2dzFCWFNZcFhleWx0MCtaRHowV2RqSjdiaEYxY1o0UG5jdlg2QUhUQjdwWXZLQ0RuR2VPd3pldEFDaVJzUTR5RjA4clN3SU51VDBiUlJwMTZ4cUxGYm1OZTYxSG5HaUtxSVRhYzJCU3Q4YTAvRjZWSUVoK3lyalhHWm5CUjJhb3pUZ0lUejloUFFraVdiQ2NSamp3ei8xeElDVFZtVFErNFJ4WVR5SkROUSszV0F6dnh3eU9nOFV2VHBrMkJQdzFqK0xrL29qVkkrcWlIMmpYS05qL0R6U1NReWkzbjlvYTFwS3VIVFZVdkhtMFpBUkNrc3hkaGN5cTJ4YmJMTWszSWRKeHRPaTk4VEloaFhYb1FQblk5TFFMK3NsK2l0VWpzTHRwSHhKOGhhdUNqNjdSK1VFWlZlbDJ3WE14aTRWTDUyT0hRcDJWQVV6TG5vV3oxeFQzS2tIQTI2RGc2aHVhdWdsRlM2ekNJZDh5L0NGYU5rQloxZ3RkQXNrdmQ5K2VzZWlXVGFaNTFxWVpCVGlsSEZkSlI3azlkbzFIQVNXUTNnbyt2SS9wL05EQVRhcFkyQTBTRFRvUVlXa1YxQ1JlQ0xWZ3ZVVENMY3c2ZnlrUy9wYVl1SVdjSHRLS1ZmQmJJUHBVM080akZtOUEyZHAzVFRZNzk3T1FzejEwZS8vRjNhc2dqVENUam9BQ0xSZXRBR01XeTFlZlVkU2ZzQmErc3NUbFJhYjBVMUNINWxKYkIvOWJVQjFkdSsxWmVyUkZYWWJ3aUQ5MytXcEY4K3VtN2ZxTTlqazJkSXNiSGhiSzJtU2FycEQ4SHRORVV6eS90TjFrK1loWTNhME9jWDFhdVhGa0NTTDlmMkRyUTRIZWg0TTRNOTRGYTFLZlN1azJtZFJYL2dOSEVrb3hLOTc5MC96bTlhWHRlcG1WaG5sdEU3SDhoNmlUQ2JzT2dTSFhrekd2TVU1TDEzMzliaGMzaVRXWVhhc2o2RlhIWjVodDJnOCtjVmFwQ0VEM2tKNmQzaEx3eWh4VFg1WDFLZ3J3Yi95Qi9ObGMyREdBNlpZSXBmeUV1THRSSlZINTZXcktCMnF2dEgxQ1RkVGlJWHZ6cVJUMkZHcU5iN3g1N1RZNUFzUFJlR05yWjY5L2ZndnRuYUNVQjc2NmMwNUxOOHZQZGs2ZTUxZUpLYXdQcWRkcEFEcE1md2QrcTVyUGkzd2FXemJSalE0ZkZESXFkdzArS3c3b2hnUy93c1dLcHA3bnFYYjN0R3ZDRTJNVnlvdlBiRFdPenYySG1jalNlNkw0WHdSaE1FNy9zTlJldUxGVEwrUG5ZTjg4bFFrRlJzM2hSdzJCbjVjMFdJZC9lOThNMlJGMm1JQ082amsvL1dYV2FvVFRaWEFrQmVxUUhjVkpodW4yMkhsVTFMK0lDM05hWVlQTURXdmUxRUpra0wrbmlJd1orQjlyUy9lL3lnZHJjRm5rQ3F4d0JXRTA4NkRYNm9oWkJDQ25FMDRlVFhsc0xBYXEvNXdBRW9qclRwMWxQcHZKMDU3d2p4ZXpQd2dmMFNmV3dCaFFoMjlnejlkdmhORzZ1L1pPY1Y3ZEQ0Tm5ldnNXNDBxWHVBMnNLenpQNDFNem5aVVI2Z3U1WHA2bnZzZUUrZ09YUTcwcER3VCtud0VzVHlPdDZlWlhoN3FkNU1vMFVKM0hYVU9DZm5VYUlzTU5zUHlpM21JTWd6YkxTQS9kTlRoeVMva3lQSXRzUlEwa040azYvWXlOdVFjbVQzNmNSZk9JYis5RTZmRjdlay96SFRlT0FMRlF5ZklvNGhuTG5FNDF5S3VOTVpPaGxLVVdHN0ZYcGIrL25NWkNtY3lVbTMwTi9lSGxqS3B4UVlxbXlsRXY3OTh6Q2daRFFBQnFUTytKdHpyOGwxekxCNnEvb3VJT3puMkJSRzJwWVZscVBHU1lOOUx1QlY0MjQ3dmZOQlRKNytwVzhoemNhbXc2T2RmY3JWNjhJWk1HandVZXFnMkNnbmVOK3YvaDZTd3dTM2xFUUZqU0xwUzJPdE9IRnhmSmtDa2dwR0JTQTVFMmtrOU1Kbjd3QXY2cEp0YlcrbHNpU3lpYTRsR2dPSWI3SlhZTnU4R0lPVWZOdnpvKzAvOGh0ODNvc29EOXByR1FUSnRiejU0YVIzc2lXcC9pVmlFbXluT0QwSzRNU3RpSk96R256ZWJ4SnBibGJqR0ZyT212OU1UZkx2R0s1ZmIxNWtESWFWcmxqVXVDaWh3dmJVcWVQcWxRMUpSMXZwNjhFa3hOM0YvVmpDQ1NHUFdCZytxa05kWXZwenQrS0UzT3g2WGRyenFIaExWRlpFUXF6R2tGdGg5MFFpNGowSThPVEh0RUJmOW5Xd3JpM0NJSlRodVBtRjZPNTZOM1hMWEZKOGxhQTZQRFBsNmMxSDV5Y0xpUWppUHN1OXNvTWY4a1J0M2pwYnExSGxyOEc5N2h1eFk5QTRweWVwMkJZYz0iLCJzIjoiZGRkNDEwYTJjYjBiYjIyYSIsIml2IjoiMmI0MGJhNDI5ZjU5YTI4YTkwMDQwZTQwMzQ5OGVhNzkifQ%3D%3D&public_key=0A1D34FC-659D-4E23-B17B-694DCFCF6A6C&site=https%3A%2F%2F103.149.200.136&userbrowser=Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F126.0.0.0%20Safari%2F537.36&capi_version=2.8.1&capi_mode=lightbox&style_theme=default&rnd=0.0926279072181091';    try {
        const response = await axios.post(
            `${getNextBaseUrl()}/fc/gt2/public_key/0A1D34FC-659D-4E23-B17B-694DCFCF6A6C`,
            data,
            {
                headers: headers,
                httpsAgent: agent,
                withCredentials: true
            }
        );

        const responseData = response.data;
        return responseData.token;
    } catch (error) {
        console.error('Error fetching token:', error.response ? error.response.data : error.message);
        return null;
    }
}

async function login(username, password, token) {
    const cookies = {
        'lb-session': '130k94c1tgeh6yd2s2ioj2ybn23alhmk',
        'xy-arkose-session': '1opgzimbsa03v0d2s2is00tn9zf0y59h',
        'timestamp': '1721245772765',
        'arkoseToken': token,
    };

    const headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'Cache-Control': 'max-age=0',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': getNextBaseUrl(),
        'Proxy-Connection': 'keep-alive',
        'Referer': `${getNextBaseUrl()}/applelogin`,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36',
    };

    const data = qs.stringify({
        'username': username,
        'password': password,
    });

    try {
        const response = await axios.post(`${getNextBaseUrl()}/applelogin`, data, {
            headers: headers,
            withCredentials: true,
            jar: cookies,
            httpsAgent: agent,
        });
        return response.data;
    } catch (error) {
        console.error('Error logging in:', error);
        return null;
    }
}

app.get('/v1/arkose', async (req, res) => {
    const token = await fetchToken();
    if (token) {
        res.json({ token: token });
    } else {
        res.status(500).json({ message: 'Failed to fetch token' });
    }
});

app.post('/auth/login', async (req, res) => {
    const { username, password } = req.body;

    if (!username || !password) {
        res.status(400).json({ message: 'Username and password are required' });
        return;
    }

    const token = await fetchToken();
    if (!token) {
        res.status(500).json({ message: 'Failed to fetch token' });
        return;
    }

    const loginResponse = await login(username, password, token);
    if (loginResponse) {
        res.json(loginResponse);
    } else {
        res.status(500).json({ message: 'Login failed' });
    }
});

app.get('/', (req, res) => {
    res.send('Hello World');
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
